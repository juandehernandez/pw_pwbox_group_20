En esta   pr�ctica de Proyectes Web II nos ped�an que hici�semos un sistema de alojamiento de documentos que sirve para que los usuarios puedan subir los documentos y archivos que se quiera, tambi�n se ped�a que fuese una interfaz simple e intuitiva.
Se han realizado todos los apartados obligatorios e incluso todos los opcionales
Ahora vamos a explicar c�mo funciona la pr�ctica:
Cuando se va al link del Pwbox, ponemos newpwbox.test en el buscador y lo primero que nos aparece es la p�gina de registro que es la que esta como landing page. En el registro el usuario tiene que introducir lo datos personales que se piden y un foto que quiera poner de perfil, sino se cargara una por defecto.
Una vez registrado, se manda un mail, al correo con el que se ha registrado, de confirmaci�n al usuario con (datos) y el link para que haga la validaci�n.
Con el link de validaci�n iremos a la p�gina de login, y el usuario se loguea con su mail o nombre de usuario y contrase�a.
Una vez dentro se pueden realizar varias tareas:
-	Update del profile de usuario Aqu� se va a poder cambiar el email, password y la imagen de perfil.
-	Delete account  Se puede borrar la cuenta del usuario si lo confirma.
-	Visualizar el contenido de su cuenta, carpetas y archivos almacenados.
-	Seg�n el rol que tenga, podr� a�adir, renombrar y borrar carpetas.
-	Seg�n el rol que tenga, podr� a�adir, renombrar y borrar archivos.
-	Puede ver y descargarse el contenido de las carpetas.
-	Pude compartir carpetas y consultar las compartidas.
La capacidad de almacenamiento de datos por usuario est� limitado a un m�ximo de 1G.
El usuario va a recibir distintas notificaciones
-	Notificaci�n de validaci�n de registro
-	Cuando un admin cambie, borre o a�ada un nuevo fichero a una carpeta al propietario de la carpeta.
-	Desde la p�gina web tambi�n podr� consultar sus notificaciones (�?)






Informaci�n relevante del desarrollo de la web
-	Carpetas
o	Cuando queremos consultar o borrar carpetas que est�n unas dentro de otras no se realiza mediante una tabla de tipo tree en la base de datos, sino que lo hacemos de manera recursiva. Tambi�n en el caso que queramos consultar los hijos de una carpeta compartida.
o	Para borrar los elementos de una carpeta, estos se van poniendo en un array de uno en uno para luego borrarlos.
o	Nos hemos dado cuenta que esta manera lo hace bien, pero consume mucha memoria, as� que en el caso de que hubiesen muchos elementos, ser�a mejor hacerlo mediante el sistema de tree conociendo los childs de cada uno de los elementos.

-	Mail
o	Para la parte del mail se ha usado la librer�a de Swift_Mailer a trav�s de un servidor de internet que se encargaba de la gesti�n de los correos.
-	Drag & Drop
o	Para realizar el drag & drop y validarlo se hace mediante una petici�n Ajax en formato Json que contiene el input file.
-	Storage space 
o	Se guarda en la tabla de user como atributo de storage. Para hacerlo se empieza desde 0 bytes y se van a poder subir archivos mientras no se supere la cantidad de 1GB en bytes, en el caso que no se pueda subir m�s se muestra error y se notifica al usuario.
-	Para el correcto funcionamiento  de Pwbox hay que cambiar los datos de la DB en App/settings.php.
-	En el login, el campo del email no se valida que se entre con un formato correcto, en caso de ser email, porqu� tambi�n se puede entrar mediante el nombre. Es decir que el pattern no es exclusivo de email.


BASE DE DATOS EN LA RAIZ IGUAL QUE EL README ARCHIVO .SQL

Juan de la Cruz 90 horas
Jordi Conesa 70 horas