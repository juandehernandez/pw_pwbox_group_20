<?php
/**
 * Created by PhpStorm.
 * User: juandelacruz
 * Date: 7/4/18
 * Time: 20:10
 */

namespace Pwbox\controller;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Psr\Container\ContainerInterface;

class GetRegisterController
{

    protected $container;


    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function __invoke(Request $request, Response $response, array $args)
    {
        return $this->container->get('view')->render($response, 'index.html.twig');
    }
}