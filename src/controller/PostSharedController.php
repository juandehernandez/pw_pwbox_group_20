<?php
/**
 * Created by PhpStorm.
 * User: juandelacruz
 * Date: 17/5/18
 * Time: 22:25
 */

namespace Pwbox\controller;


use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Psr\Container\ContainerInterface;
use PDOException;

class PostSharedController
{

    /**
     * @var
     */
    protected $container;


    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function __invoke(Request $request, Response $response, array $args)
    {
        try {

            $errors = [];

            $data = $request->getParsedBody();

            $activeModal = '';

            $isAdmin = FALSE;

            if (isset($args['folder'])) {
                $service = $this->container->get('get_shared_owner_service');
                $ownerId = $service($args['folder']);

                $isAdmin = $service($args['folder'], $_SESSION['id']);

                $directory = $this->container->get('upload_directory');
                $ownerdirectory = $directory . DIRECTORY_SEPARATOR . $ownerId;
            }

            //New folder case
            if (isset($data['folder'])) {
                if (empty($data['folder'])) {
                    $activeModal = 'folderModal';
                    $errors['folder'] = "Folder name cannot be empty";
                } else {
                    $service = $this->container->get('upload_folder_service');
                    if (isset($args['folder'])) {
                        $service($ownerId, $data['folder'], 0, $args['folder']);
                    }
                    $notificationMessage = 'added new folder';
                }

            } else if(!empty($request->getUploadedFiles())) {
                //case File upload
                $service = $this->container->get('upload_file_service');
                if (isset($args['folder'])) {
                    $errors = $service($ownerId, $request->getUploadedFiles(), $ownerdirectory, "fileupload", 0, $args['folder']);
                }

                if (!empty($errors)) {
                    $newResponse = $response->withJson($errors[0], 500);
                    return $newResponse;

                }

                $notificationMessage = 'uploaded new file';

                return $response
                    ->withStatus(200);

            } else if (isset($data['deleteDirId'])) {
                //case remove dir
                $service = $this->container->get('get_shared_owner_service');
                $ownerId = $service($data['deleteDirId']);

                $service = $this->container->get('remove_dir_service');
                $service($ownerId, $data['deleteDirId'], $ownerdirectory);

                $notificationMessage = 'removed dir';
            } else if (isset($data['downloadDirId'])) {
                //case download file
                $file = __DIR__ . '/../../public/userinfo/' . $_SESSION['id'] . '/' . $data['downloadDirId'];
                $fh = fopen($file, 'rb');

                $stream = new \Slim\Http\Stream($fh); // create a stream instance for the response body

                return $response->withHeader('Content-Type', 'application/force-download')
                    ->withHeader('Content-Type', 'application/octet-stream')
                    ->withHeader('Content-Type', 'application/download')
                    ->withHeader('Content-Description', 'File Transfer')
                    ->withHeader('Content-Transfer-Encoding', 'binary')
                    ->withHeader('Content-Disposition', 'attachment; filename="' . basename($file) . '"')
                    ->withHeader('Expires', '0')
                    ->withHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0')
                    ->withHeader('Pragma', 'public')
                    ->withBody($stream); // all stream contents will be sent to the response
            } else if (isset($data['renameDirId'])) {
                //case rename file or folder
                $service = $this->container->get('update_dir_name_service');
                $service($data);
            }

            $isRoot = FALSE;
            if (isset($args['folder'])) {
                $service = $this->container->get('get_shared_dirs_by_parent_service');
                $dirs = $service($_SESSION['id'], $args['folder']);
            } else {
                $service = $this->container->get('get_shared_dirs_service');
                $dirs = $service($_SESSION['id']);
                $isRoot = TRUE;
            }

            if ($dirs != null) {
                foreach($dirs as $key=>$dir){
                    if ($dir['type_id'] == 1) {
                        $dirs[$key]['id'] = $dir['id'];
                        $dirs[$key]['type'] = "folder-open";
                    } else {
                        $dirs[$key]['id'] = $dir['dir_name'];
                        $dirs[$key]['type'] = "file";
                    }
                }
            }

            $service = $this->container->get('get_user_service');
            $user = $service($_SESSION['id']);

            if (empty($user['profile_image'])) {
                $info['profileImageSrc'] = $this->container->get("web_default_avatar");
            } else {
                $info['profileImageSrc'] = $this->container->get("web_profile_images") . DIRECTORY_SEPARATOR . $user['profile_image'];
            }

            //Send notification to the owner of shared dirs if there is a Notification Message
            if (isset($notificationMessage)) {
                $message = $user['username'] . ' ' . $notificationMessage;
                $newNotificationService = $this->container->get('new_notification_service');
                $newNotificationService($_SESSION['id'], $message);

                $service = $this->container->get('get_user_service');
                $sharedOwner = $service($ownerId);

                $sendMailService = $this->container->get('send_mail_service');
                $sendMailService($sharedOwner['username'], $sharedOwner['email'], $message);
            }

            return $this->container->get('view')

                ->render($response, 'dashboard.html.twig', ['errors' => $errors, 'isPost' => true, 'dirs' => $dirs,  'user' => $user,
                    'data' => $data, 'activeModal'=> $activeModal, 'isRoot' => $isRoot, 'isAdmin' => $isAdmin]);


        } catch (\PDOException $e) {
            $response = $response
                ->withStatus(500)
                ->withHeader('Content-type', 'text/html')
                ->write($e->getMessage());
        } catch (\Exception $e) {
            $response = $response
                ->withStatus(500)
                ->withHeader('Content-type', 'text/html')
                ->write($e->getMessage());
        }
    }
}