<?php
/**
 * Created by PhpStorm.
 * User: juandelacruz
 * Date: 7/4/18
 * Time: 20:10
 */

namespace Pwbox\controller;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Psr\Container\ContainerInterface;
use PDOException;

class PostLoginController
{

    /**
     * @var
     */
    protected $container;


    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function __invoke(Request $request, Response $response, array $args)
    {
        try {
            $data = $request->getParsedBody();
            if (isset($data['email']) && isset($data['password'])) {
                $email = $data['email'];
                $password = $data['password'];

                $errors = [];

                /*if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $errors['email'] = "It must be a valid email";
                }*/

                preg_match_all("/[A-Z]/", $password, $caps_match);
                $caps_count = count($caps_match [0]);

                preg_match_all("/[0-9]/", $password, $nums_match);
                $nums_count = count($nums_match [0]);

                if (strlen($password) > 12 || strlen($password) < 6 || $caps_count == 0 || $nums_count == 0) {
                    $errors['password'] = "The length of the password must be between 6 and 12 characters and it must contain at least one number and one upper case letter";
                }

                if (empty($errors)) {
                    $service = $this->container->get('login_user_service');
                    $user = $service($data);
                    if (empty($user)) {
                        $errors['usernoexist'] = "There is no user related to that password";
                        return $this->container->get("view")->render($response, 'login.html.twig', ['errors' => $errors, 'data' => $data]);
                    }

                    $_SESSION['id'] = $user['id'];
                    $this->container->get('flash')->addMessage('user_login', 'The user has been succesfully logged in');
                    return $response->withStatus(302)->withHeader('Location', '/dashboard');

                } else {
                    return $this->container->get("view")->render($response, 'login.html.twig', ['errors' => $errors, 'data' => $data]);
                }
            }

        } catch (PDOException $e) {
            $response = $response
                ->withStatus(500)
                ->withHeader('Content-type', 'text/html')
                ->write($e->getMessage());
        } catch (\Exception $e) {
            $response = $response
                ->withStatus(500)
                ->withHeader('Content-type', 'text/html')
                ->write($e->getMessage());
        }
    }
}