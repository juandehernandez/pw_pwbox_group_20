<?php
/**
 * Created by PhpStorm.
 * User: juandelacruz
 * Date: 13/4/18
 * Time: 23:45
 */

namespace Pwbox\controller;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Psr\Container\ContainerInterface;

class GetProfileController
{

    protected $container;


    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function __invoke(Request $request, Response $response, array $args)
    {
        try {
            $data = $request->getParsedBody();

            $service = $this->container->get('get_user_service');
            $user = $service($_SESSION['id']);
            if (empty($user['profile_image'])) {
                $user['profileImageSrc'] = $this->container->get("web_default_avatar");
            } else {
                $user['profileImageSrc'] = $this->container->get("web_profile_images") . DIRECTORY_SEPARATOR . $user['profile_image'];
            }

            return $this->container->get('view')->render($response, 'profile.html.twig', ['user' => $user, 'showVerifyMessage' => true]);

        } catch (PDOException $e) {
            $response = $response
                ->withStatus(500)
                ->withHeader('Content-type', 'text/html')
                ->write($e->getMessage());
        } catch (\Exception $e) {
            $response = $response
                ->withStatus(500)
                ->withHeader('Content-type', 'text/html')
                ->write($e->getMessage());
        }
    }
}