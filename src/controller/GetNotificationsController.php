<?php
/**
 * Created by PhpStorm.
 * User: juandelacruz
 * Date: 18/5/18
 * Time: 14:16
 */

namespace Pwbox\controller;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use Psr\Container\ContainerInterface;

class GetNotificationsController
{

    protected $container;


    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function __invoke(Request $request, Response $response, array $args)
    {
        $service = $this->container->get('get_user_service');
        $user = $service($_SESSION['id']);
        if (empty($user['profile_image'])) {
            $user['profileImageSrc'] = $this->container->get("web_default_avatar");
        } else {
            $user['profileImageSrc'] = $this->container->get("web_profile_images") . DIRECTORY_SEPARATOR . $user['profile_image'];
        }

        $service = $this->container->get('get_notifications_service');
        $notifications = $service($_SESSION['id']);

        return $this->container->get('view')->render($response, 'notifications.html.twig', ['user' => $user, 'notifications' => $notifications]);
    }
}