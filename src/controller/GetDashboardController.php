<?php
/**
 * Created by PhpStorm.
 * User: juandelacruz
 * Date: 14/4/18
 * Time: 20:30
 */

namespace Pwbox\controller;

use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Psr\Container\ContainerInterface;

class GetDashboardController
{

    protected $container;


    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function __invoke(Request $request, Response $response, array $args)
    {
        $isRoot = FALSE;

        if (isset($args['folder'])) {
            $service = $this->container->get('get_dirs_by_parent_service');
            $dirs = $service($_SESSION['id'], $args['folder']);
        } else {
            $service = $this->container->get('get_root_dirs_service');
            $dirs = $service($_SESSION['id']);
            $isRoot = TRUE;
        }

        foreach($dirs as $key=>$dir){
            if ($dir['type_id'] == 1) {
                $dirs[$key]['id'] = $dir['id'];
                $dirs[$key]['type'] = "folder-open";
            } else {
                $dirs[$key]['id'] = $dir['dir_name'];
                $dirs[$key]['type'] = "file";
            }
        }

        $service = $this->container->get('get_user_service');
        $user = $service($_SESSION['id']);
        if (empty($user['profile_image'])) {
            $user['profileImageSrc'] = $this->container->get("web_default_avatar");
        } else {
            $user['profileImageSrc'] = $this->container->get("web_profile_images") . DIRECTORY_SEPARATOR . $user['profile_image'];
        }


        return $this->container->get('view')->render($response, 'dashboard.html.twig', ['dirs' => $dirs, 'user' => $user, 'isRoot' => $isRoot]);
    }
}