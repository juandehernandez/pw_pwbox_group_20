<?php
/**
 * Created by PhpStorm.
 * User: juandelacruz
 * Date: 19/4/18
 * Time: 18:43
 */

namespace Pwbox\controller\Middleware;

use Psr\Container\ContainerInterface;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class UserLoggedMiddleware
{
    protected $container;


    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function __invoke(Request $request, Response $response, callable $next)
    {
        if (!isset($_SESSION['id'])) {
            return $response->withStatus(302)->withHeader('Location', '/');
        }
        return $next($request, $response);
    }

}