<?php
/**
 * Created by PhpStorm.
 * User: jordi_3
 * Date: 26/04/2018
 * Time: 18:55
 */

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Psr\Container\ContainerInterface;

class FileController{
    protected $container;


    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }



    public function uploadFileAction(Request $request, Response $response)

    {
        //Toda la url del servidor que va a ser ambsoluta.
        $directory = '/home/vagrant/projects/pwbox/public/uploads';
       //Nos pasa un array, con los fitxero
        $uploadedFiles = $request->getUploadedFiles();
        $errors = [];
        foreach ($uploadedFiles['files'] as $uploadedFile) {
            if ($uploadedFile->getError() !== UPLOAD_ERR_OK) {
                $errors[] = sprintf(
                    'An unexpected error ocurred uploading the file %s',
                    $uploadedFile->getClientFilename()
                );
                continue;
            }
            $fileName = $uploadedFile->getClientFilename();
            $fileInfo = pathinfo($fileName);
            $extension = $fileInfo['extension'];
            if (!$this->isValidExtension($extension)) {
                $errors[] = sprintf(
                    'Unable to upload the file %s, the extension %s is not valid',
                    $fileName,
                    $extension
                );
                continue;
            }
            //        $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $fileName);
        }
        return $this->container->get('view')
            ->render($response, 'file.twig', ['errors' => $errors, 'isPost' => true]);
    }
    /**
     * Validate the extension of the file
     *
     * @param string $extension
     * @return boolean
     */
    private function isValidExtension(string $extension)
    {
        //en este array hay que poner las extensiones que son validas
        $validExtensions = ['jpg', 'png'];
        return in_array($extension, $validExtensions);
    }
}
