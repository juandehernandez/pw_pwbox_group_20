<?php
/**
 * Created by PhpStorm.
 * User: juandelacruz
 * Date: 13/4/18
 * Time: 22:19
 */

namespace Pwbox\controller;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use Psr\Container\ContainerInterface;

class GetLoginController
{

    protected $container;


    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function __invoke(Request $request, Response $response, array $args)
    {
        $activeModal = '';

        if (!empty($this->container->get('flash')->getMessages())) {
            $activeModal = 'verificationModal';
        }
        return $this->container->get('view')->render($response, 'login.html.twig', ['activeModal' => $activeModal]);
    }
}