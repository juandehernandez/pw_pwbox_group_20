<?php
/**
 * Created by PhpStorm.
 * User: juandelacruz
 * Date: 8/5/18
 * Time: 19:36
 */

namespace Pwbox\controller;


use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Psr\Container\ContainerInterface;

class GetSharedController
{

    protected $container;


    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function __invoke(Request $request, Response $response, array $args)
    {
        try {
            $data = $request->getParsedBody();
            $activeModal = '';

            $isAdmin = FALSE;

            $isRoot = FALSE;
            if (isset($args['folder'])) {
                $service = $this->container->get('get_shared_dirs_by_parent_service');
                $dirs = $service($_SESSION['id'], $args['folder']);

                //Check if user is role admin
                $service = $this->container->get('is_admin_role_service');
                $isAdmin = $service($args['folder'], $_SESSION['id']);
            } else {
                $service = $this->container->get('get_shared_dirs_service');
                $dirs = $service($_SESSION['id']);
                $isRoot = TRUE;
            }

            $service = $this->container->get('get_user_service');
            $user = $service($_SESSION['id']);

            if (empty($user['profile_image'])) {
                $user['profileImageSrc'] = $this->container->get("web_default_avatar");
            } else {
                $user['profileImageSrc'] = $this->container->get("web_profile_images") . DIRECTORY_SEPARATOR . $user['profile_image'];
            }

            if ($dirs != null) {
                foreach($dirs as $key=>$dir){
                    if ($dir['type_id'] == 1) {
                        $dirs[$key]['id'] = $dir['id'];
                        $dirs[$key]['type'] = "folder-open";
                    } else {
                        $dirs[$key]['id'] = $dir['dir_name'];
                        $dirs[$key]['type'] = "file";
                    }
                }
            }



            return $this->container->get('view')->render($response, 'shared.html.twig', ['user' => $user, 'dirs' => $dirs, 'isRoot' => $isRoot, 'isAdmin' => $isAdmin]);

        } catch (PDOException $e) {
            $response = $response
                ->withStatus(500)
                ->withHeader('Content-type', 'text/html')
                ->write($e->getMessage());
        } catch (\Exception $e) {
            $response = $response
                ->withStatus(500)
                ->withHeader('Content-type', 'text/html')
                ->write($e->getMessage());
        }
    }
}