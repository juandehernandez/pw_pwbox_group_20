<?php
/**
 * Created by PhpStorm.
 * User: juandelacruz
 * Date: 14/5/18
 * Time: 14:45
 */

namespace Pwbox\controller;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Psr\Container\ContainerInterface;

class MailActivationController
{

    protected $container;


    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function __invoke(Request $request, Response $response, array $args)
    {

        $checkVerificationService = $this->container->get('check_verification_service');
        $userId = $checkVerificationService($args['key']);

        if (!$userId) {
            echo("key not valid");
        } else {
            $_SESSION['id'] = $userId;
            $updateVerifiedService = $this->container->get('update_verified_service');
            $updateVerifiedService($userId);

            $this->container->get('flash')->addMessage('user_verify', 'The email has been succesfully verified');

            return $response->withStatus(302)->withHeader('Location', '/dashboard');
        }

    }
}