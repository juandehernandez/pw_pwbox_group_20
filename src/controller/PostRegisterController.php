<?php
/**
 * Created by PhpStorm.
 * User: juandelacruz
 * Date: 7/4/18
 * Time: 20:10
 */

namespace Pwbox\controller;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Psr\Container\ContainerInterface;
use PDOException;

class PostRegisterController
{

    /**
     * @var
     */
    protected $container;


    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function __invoke(Request $request, Response $response, array $args)
    {
        try {
            $data = $request->getParsedBody();

            if (isset($data['username']) && isset($data['email']) && isset($data['birthdate'])
                && isset($data['password']) && isset($data['confirmPassword'])) {

                $username = $data['username'];
                $email = $data['email'];
                $birthdate = $data['birthdate'];
                $password = $data['password'];
                $confirmPassword = $data['confirmPassword'];

                $errors = [];

                if (empty($username) || strlen($username) > 20) {
                    $errors['username'] = "Username can only contain alphanumeric characters and a max length of 20 characters";
                }

                if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $errors['email'] = "It must be a valid email";
                }

                if (empty($birthdate)) {
                    $errors['birthdate'] = "It must be a valid date and must be well formatted";
                }

                preg_match_all("/[A-Z]/", $password, $caps_match);
                $caps_count = count($caps_match [0]);

                preg_match_all("/[0-9]/", $password, $nums_match);
                $nums_count = count($nums_match [0]);

                if (strlen($password) > 12 || strlen($password) < 6 || $caps_count == 0 || $nums_count == 0) {
                    $errors['password'] = "The length of the password must be between 6 and 12 characters and it must contain at least one number and one upper case letter";
                }

                if ($password != $confirmPassword) {
                    $errors['confirmPassword'] = "It must match with the value of the password field";
                }

                $checkRegisteredUserService = $this->container->get('check_registered_user_service');
                $isRegisteredUser = $checkRegisteredUserService($username);
                if($isRegisteredUser) {
                    $errors['registeredUser'] = "User is already registered";
                }

                if (empty($errors)) {
                    //Se sube la profile image del usuario
                    //Comprobamos si el usuario no ha escogido imagen de perfil, entonces le damos la default
                    if ($request->getUploadedFiles()['files'][0]->getError() != 4) {
                        //Error 4 es no file selected, si es diferente de 4 entonces si el usuario escogió imagen
                        $uploadFileService = $this->container->get('upload_file_service');
                        $profileImagesDir = $this->container->get('profile_images_directory');
                        $userProfileImage = $uploadFileService($_SESSION['id'], $request->getUploadedFiles(), $profileImagesDir, "profileimage");

                        if (is_array($userProfileImage)) {
                            return $this->container->get("view")->render($response, 'index.html.twig', ['profileImageError' => $userProfileImage[0], 'data' => $data]);
                        }

                    } else {
                        $userProfileImage = '';
                    }

                    //Se crea el usuario
                    $postUserService = $this->container->get('post_user_service');
                    $userId = $postUserService($data, $userProfileImage);

                    //Se crea su carpeta para subir archivos
                    $directory = $this->container->get('upload_directory');
                    $userdirectory = $directory . DIRECTORY_SEPARATOR . $userId;

                    mkdir($userdirectory, 0777, true);

                    //Ahora tema verificación email
                    $verificationKey = md5($username . $email);

                    $sendMailService = $this->container->get('send_mail_service');
                    $message = 'Hello ' . $username . '<a href="http://pwbox.test/mail_activation/' . $verificationKey . '"> Click here to verificate your account</a>';
                    $sendMailService($username, $email, $verificationKey, $message);

                    $postVerificationKeyService = $this->container->get('post_verification_key_service');
                    $postVerificationKeyService($userId, $verificationKey);

                    $this->container->get('flash')->addMessage('user_register', 'The user has been succesfully registered');

                    return $response->withStatus(302)->withHeader('Location', '/login');

                }

                return $this->container->get("view")->render($response, 'index.html.twig', ['errors' => $errors, 'data' => $data]);
            }
        } catch (PDOException $e) {
            $response = $response
                ->withStatus(500)
                ->withHeader('Content-type', 'text/html')
                ->write($e->getMessage());

        } catch (\Exception $e) {
            $response = $response
                ->withStatus(500)
                ->withHeader('Content-type', 'text/html')
                ->write($e->getMessage());
        }
    }
}