<?php
/**
 * Created by PhpStorm.
 * User: juandelacruz
 * Date: 11/4/18
 * Time: 19:05
 */

namespace Pwbox\controller;


use \psr\Http\Message\ServerRequestInterface as Request;


use Dflydev\FigCookies\FigRequestCookies;
use Dflydev\FigCookies\FigResponseCookies;
use Dflydev\FigCookies\SetCookie;


use \Psr\Http\Message\ResponseInterface as Response;
use Psr\Container\ContainerInterface;
use Dflydev\FigCookies\Cookie;



class HelloController
{
    protected $container;


    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function __invoke(Request $request, Response $response, array $args)
    {
        if (!isset($_SESSION['counter'])) {
            $_SESSION['counter'] = 1;
        } else {
            $_SESSION['counter'] += 1;
        }

        $cookie = FigRequestCookies::get($request, 'advice', 0);

        if (empty($cookie->getValue())) {
            die('ok');
            $response = FigResponseCookies::set($response, SetCookie::create('advice')
                ->withValue(1)
                ->withDomain('pwbox.test')
                ->withPath('/')
            );

        }

        $name = $args['name'];

        if (!isset($_SESSION['counter'])) {
            $_SESSION['counter'] = 1;
        }else{
            $_SESSION['counter'] += 1;
        }
        $cookie = Cookie::create('theme', 'blue');
        if(empty($cookie->getValue())){
            $response = FigResponseCookies::set($response, SetCookie::create('advice')
                ->withValue('1')
                ->withDomain('newpwbox.test')
                ->withPath('/')
            );
        }
        $name = $args['name'];

        return $this->container->get('view')
            ->render($response, 'index.html.twig', [
                'name' => $name,
                'counter' => $_SESSION['counter']
            ]);
    }
}