<?php
/**
 * Created by PhpStorm.
 * User: juandelacruz
 * Date: 20/4/18
 * Time: 23:48
 */

namespace Pwbox\controller;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Psr\Container\ContainerInterface;

class PostDashboardController
{
    /**
     * @var
     */
    protected $container;


    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function __invoke(Request $request, Response $response, array $args)
    {
        try {

            $errors = [];

            $data = $request->getParsedBody();

            $activeModal = '';
            $directory = $this->container->get('upload_directory');
            $userdirectory = $directory . DIRECTORY_SEPARATOR . $_SESSION['id'];

            //New folder case
            if (isset($data['folder'])) {
                if (empty($data['folder'])) {
                    $activeModal = 'folderModal';
                    $errors['folder'] = "Folder name cannot be empty";
                } else {
                    $service = $this->container->get('upload_folder_service');
                    if (isset($args['folder'])) {
                        $service($_SESSION['id'], $data['folder'], 0, $args['folder']);
                        //Es fa un refresh de la pàgina perquè al pujar files via ajax
                        // es queda activat el isset de data folder i torna a crear una altra.
                        // D'aquesta manera evitem el problema.
                        return $response->withStatus(302)->withHeader('Location', '/dashboard/' . $args['folder']);
                    } else {
                        $service($_SESSION['id'], $data['folder'], 1);
                        return $response->withStatus(302)->withHeader('Location', '/dashboard');
                    }
                }

            }else if (isset($data['role']) && isset($data['email'])) {
                //Share folder case
                if (empty($data['email'])) {
                    $activeModal = 'shareModal';
                    $errors['email'] = "Email cannot be empty";
                } else if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
                    $activeModal = 'shareModal';
                    $errors['email'] = "Invalid email format";
                } else {
                    $service = $this->container->get('check_registered_email_service');
                    $isRegisteredEmail = $service($data['email']);

                    if (!$isRegisteredEmail) {
                        $activeModal = 'shareModal';
                        $errors['email'] = "This email is not registered";
                    } else {
                        $getUserIdService = $this->container->get('get_userid_by_email_service');
                        $data['userId'] = $getUserIdService($data);

                        $service = $this->container->get('user_has_role_service');
                        $userHasRole = $service($data);

                        if ($userHasRole) {
                            $errors['role'] = "The user already have this role in that directory";
                        }
                    }
                }

                if (empty($errors)) {
                    $shareDirService = $this->container->get('share_dir_service');
                    $shareDirService($data);
                } else {
                    $activeModal = 'shareModal';
                }
            } else if(!empty($request->getUploadedFiles())) {
                //case File upload
                $service = $this->container->get('upload_file_service');
                if (isset($args['folder'])) {
                    $errors = $service($_SESSION['id'], $request->getUploadedFiles(), $userdirectory, "fileupload", 0, $args['folder']);
                } else {
                    $errors = $service($_SESSION['id'], $request->getUploadedFiles(), $userdirectory, "fileupload", 1);
                }

                if (!empty($errors)) {
                    $newResponse = $response->withJson($errors[0], 500);
                    return $newResponse;

                }

                return $response
                    ->withStatus(200);

            } else if (isset($data['deleteDirId'])) {
                //case remove dir
                $service = $this->container->get('remove_dir_service');
                $service($_SESSION['id'], $data['deleteDirId'], $userdirectory);

            } else if (isset($data['downloadDirId'])) {
                //case download file
                $file = __DIR__ . '/../../public/userinfo/' . $_SESSION['id'] . '/' . $data['downloadDirId'];
                $fh = fopen($file, 'rb');

                $stream = new \Slim\Http\Stream($fh); // create a stream instance for the response body

                return $response->withHeader('Content-Type', 'application/force-download')
                    ->withHeader('Content-Type', 'application/octet-stream')
                    ->withHeader('Content-Type', 'application/download')
                    ->withHeader('Content-Description', 'File Transfer')
                    ->withHeader('Content-Transfer-Encoding', 'binary')
                    ->withHeader('Content-Disposition', 'attachment; filename="' . basename($file) . '"')
                    ->withHeader('Expires', '0')
                    ->withHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0')
                    ->withHeader('Pragma', 'public')
                    ->withBody($stream); // all stream contents will be sent to the response
            } else if (isset($data['renameDirId'])) {
                //case rename file or folder
                $service = $this->container->get('update_dir_name_service');
                $service($data);
            }

            $isRoot = FALSE;
            if (isset($args['folder'])) {
                $service = $this->container->get('get_dirs_by_parent_service');
                $dirs = $service($_SESSION['id'], $args['folder']);
            } else {
                $service = $this->container->get('get_root_dirs_service');
                $dirs = $service($_SESSION['id']);
                $isRoot = TRUE;
            }

            foreach($dirs as $key=>$dir){
                if ($dir['type_id'] == 1) {
                    $dirs[$key]['id'] = $dir['id'];
                    $dirs[$key]['type'] = "folder-open";
                } else {
                    $dirs[$key]['id'] = $dir['dir_name'];
                    $dirs[$key]['type'] = "file";
                }
            }

            $service = $this->container->get('get_user_service');
            $user = $service($_SESSION['id']);

            if (empty($user['profile_image'])) {
                $user['profileImageSrc'] = $this->container->get("web_default_avatar");
            } else {
                $user['profileImageSrc'] = $this->container->get("web_profile_images") . DIRECTORY_SEPARATOR . $user['profile_image'];
            }

            return $this->container->get('view')

                ->render($response, 'dashboard.html.twig', ['errors' => $errors, 'isPost' => true, 'dirs' => $dirs,  'user' => $user, 'data' => $data, 'activeModal'=> $activeModal, 'isRoot' => $isRoot]);


        } catch (\PDOException $e) {
            $response = $response
                ->withStatus(500)
                ->withHeader('Content-type', 'text/html')
                ->write($e->getMessage());
        } catch (\Exception $e) {
            $response = $response
                ->withStatus(500)
                ->withHeader('Content-type', 'text/html')
                ->write($e->getMessage());
        }
    }

}