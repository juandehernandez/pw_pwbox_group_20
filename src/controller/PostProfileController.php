<?php
/**
 * Created by PhpStorm.
 * User: juandelacruz
 * Date: 13/4/18
 * Time: 23:47
 */

namespace Pwbox\controller;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Psr\Container\ContainerInterface;
use PDOException;

class PostProfileController
{
    /**
     * @var
     */
    protected $container;


    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function __invoke(Request $request, Response $response, array $args)
    {
        try {
            $data = $request->getParsedBody();

            $disableModal = '';

            $data['id'] = $_SESSION['id'];

            if (isset($data['email'])) {
                $email = $data['email'];

                $errors = [];

                if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $errors['email'] = "It must be a valid email";
                }

                if (empty($errors)) {
                    $service = $this->container->get('update_email_service');
                    $service($data);
                    $disableModal = 'emailModal';
                }

            } else if (isset($data['newPassword']) && isset($data['confirmPassword'])) {
                $password = $data['newPassword'];
                $confirmPassword = $data['confirmPassword'];

                $errors = [];
                preg_match_all("/[A-Z]/", $password, $caps_match);
                $caps_count = count($caps_match [0]);

                preg_match_all("/[0-9]/", $password, $nums_match);
                $nums_count = count($nums_match [0]);

                if (strlen($password) > 12 || strlen($password) < 6 || $caps_count == 0 || $nums_count == 0) {
                    $errors['newPassword'] = "The length of the password must be between 6 and 12 characters and it must contain at least one number and one upper case letter";
                }

                if ($password != $confirmPassword) {
                    $errors['confirmPassword'] = "It must match with the value of the password field";
                }

                if (empty($errors)) {
                    $service = $this->container->get('update_password_service');
                    $service($data);
                    $disableModal = 'passwordModal';
                }

            } else if (isset($data['deleteAccount'])) {
                $service = $this->container->get('delete_user_service');
                $service($data);

                return $response->withStatus(302)->withHeader('Location', '/logout');

            } else if(!empty($request->getUploadedFiles())) {
                //case File upload
                //Pujar nou file
                $uploadFileService = $this->container->get('upload_file_service');
                $profileImagesDir = $this->container->get('profile_images_directory');
                $userProfileImage = $uploadFileService($_SESSION['id'], $request->getUploadedFiles(), $profileImagesDir, "profileimage");

                if (is_array($userProfileImage)) {
                    $errors['fileError'] = $userProfileImage[0];
                    $disableModal = 'filesModal';
                } else {
                    //Borrar profile image antiga
                    $getUserProfileImageService = $this->container->get('get_user_profile_image_service');
                    $actualProfileImage = $getUserProfileImageService($_SESSION['id']);
                    $profileImagesDir = $this->container->get('profile_images_directory');
                    unlink($profileImagesDir . DIRECTORY_SEPARATOR . $actualProfileImage);

                    //Actualitzar profile_image de tabla user
                    $updateProfileImageService = $this->container->get('update_profile_image_service');
                    $updateProfileImageService($_SESSION['id'], $userProfileImage);
                }
            } else if(isset($data['sendEmail'])) {
                //Ahora tema verificación email
                $service = $this->container->get('get_user_service');
                $user = $service($_SESSION['id']);

                $verificationKey = md5($user['username'] . $user['email']);

                $sendMailService = $this->container->get('send_mail_service');
                $message = 'Hello ' . $user['username'] . '<a href="http://pwbox.test/mail_activation/' . $verificationKey . '"> Click here to verificate your account</a>';
                $sendMailService($user['username'], $user['email'], $message);

                $postVerificationKeyService = $this->container->get('post_verification_key_service');
                $postVerificationKeyService($_SESSION['id'], $verificationKey);


                if (empty($user['profile_image'])) {
                    $user['profileImageSrc'] = $this->container->get("web_default_avatar");
                } else {
                    $user['profileImageSrc'] = $this->container->get("web_profile_images") . DIRECTORY_SEPARATOR . $user['profile_image'];
                }

                return $this->container->get('view')->render($response, 'profile.html.twig', ['user' => $user, 'emailSent' => true]);
            }

            if (empty($errors)) {
                $newResponse = $response->withJson($disableModal, 200);
                return $newResponse;
            } else {
                $newResponse = $response->withJson($errors, 500);
                return $newResponse;
            }

        } catch (PDOException $e) {
            $response = $response
                ->withStatus(500)
                ->withHeader('Content-type', 'text/html')
                ->write($e->getMessage());
        } catch (\Exception $e) {
            $response = $response
                ->withStatus(500)
                ->withHeader('Content-type', 'text/html')
                ->write($e->getMessage());
        }
    }


}