<?php
/**
 * Created by PhpStorm.
 * User: juandelacruz
 * Date: 7/4/18
 * Time: 22:52
 */

namespace Pwbox\model;


interface UserRepository
{
    public function register(User $user);

    public function login($email, $password);

    public function updateEmail($id, $email);

    public function updatePassword($id, $password);

    public function getUserById($id);

    public function insertDir(Dir $dir);

    public function getRootDirs($userId);

    public function getDirsByParent($userId, $parentId);

    public function checkRegisteredUser($username);

    public function checkRegisteredEmail($email);

    public function shareDir($dirId, $userId, $role);

    public function getUserIdByEmail($email);

    public function getSharedDirs($userId);

    public function isSharedDirsByParent($userId, $parentId, $role);

    public function getDirParentId($dirId);

    public function deleteDir($dirId, $type);

    public function deleteUser($userId);

    public function deleteUserDirs($userId);

    public function insertVerificationKey($userId, $key);

    public function checkVerification($key);

    public function updateVerified($userId);

    public function updateProfileImage($userId, $profileImage);

    public function getUserProfileImage($userId);

    public function userHasRole($userId, $dirId, $roleId);

    public function incrementStorage($userId, $fileSize);

    public function decrementStorage($userId, $fileSize);

    public function newNotification($userId, $message);

    public function getNotifications($userId);

    public function getSharedOwner($parentId);

    public function updateDirName($dirId, $name, $type);

    public function isAdminRole($parentId, $userId);

    public function deleteUserShares($userId);

    public function deleteUserVerificationKeys($userId);
}