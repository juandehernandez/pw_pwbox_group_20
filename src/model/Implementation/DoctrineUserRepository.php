<?php
/**
 * Created by PhpStorm.
 * User: juandelacruz
 * Date: 12/4/18
 * Time: 19:14
 */

namespace Pwbox\model\Implementation;


use Doctrine\DBAL\Connection;
use Pwbox\model\Dir;
use Pwbox\model\User;
use Pwbox\model\UserRepository;

class DoctrineUserRepository implements UserRepository
{

    private const DATE_FORMAT = 'Y-m-d H:i:s';
    private $database;

    public function __construct(Connection $database)
    {
        $this->database = $database;
    }

    public function register(User $user)
    {
        $sql = "INSERT INTO user(username, email, password, birthdate, created_at, updated_at, profile_image) VALUES(:username, :email, :password, :birthdate, :created_at, :updated_at, :profile_image)";
        $stmt = $this->database->prepare($sql);
        $stmt->bindValue("username", $user->getUsername(), 'string');
        $stmt->bindValue("email", $user->getEmail(), 'string');
        $stmt->bindValue("password", $user->getPassword(), 'string');
        $stmt->bindValue("birthdate", $user->getBirthdate()->format(self::DATE_FORMAT));
        $stmt->bindValue("created_at", $user->getCreatedAt()->format(self::DATE_FORMAT));
        $stmt->bindValue("updated_at", $user->getCreatedAt()->format(self::DATE_FORMAT));
        $stmt->bindValue("profile_image", $user->getProfileImage(), 'string');
        $stmt->execute();

        return $this->database->lastInsertId();
    }

    public function login($email, $password)
    {
        $sql = "SELECT * FROM user WHERE email = :email AND password = :password";
        $stmt = $this->database->prepare($sql);
        $stmt->bindValue("email", $email, 'string');
        $stmt->bindValue("password", $password, 'string');
        $stmt->execute();
        $user = $stmt->fetch();

        if (empty($user)) {
            $sql = "SELECT * FROM user WHERE username = :username AND password = :password";
            $stmt = $this->database->prepare($sql);
            $stmt->bindValue("username", $email, 'string');
            $stmt->bindValue("password", $password, 'string');
            $stmt->execute();
            $user = $stmt->fetch();
        }


        return $user;
    }

    public function getUserById($id) {
        $sql = "SELECT * FROM user WHERE id = :id";
        $stmt = $this->database->prepare($sql);
        $stmt->bindValue("id", $id, 'integer');
        $stmt->execute();
        $user = $stmt->fetch();

        return $user;
    }

    public function updateEmail($id, $email)
    {
        $sql = "UPDATE user SET email = :email WHERE id = :id";
        $stmt = $this->database->prepare($sql);
        $stmt->bindValue("email", $email, 'string');
        $stmt->bindValue("id", $id, 'integer');
        $stmt->execute();
    }

    public function updatePassword($id, $password)
    {
        $sql = "UPDATE user SET password = :password WHERE id = :id";
        $stmt = $this->database->prepare($sql);
        $stmt->bindValue("password", $password, 'string');
        $stmt->bindValue("id", $id, 'integer');
        $stmt->execute();
    }

    public function insertDir(Dir $dir)
    {
        $sql = "INSERT INTO dir(user_id, name, dir_name, is_root, parent_id, type_id) VALUES(:user_id, :name, :dir_name, :is_root, :parent_id, :type_id)";
        $stmt = $this->database->prepare($sql);
        $stmt->bindValue("user_id", $dir->getUserId(), 'integer');
        $stmt->bindValue("name", $dir->getName(), 'string');
        $stmt->bindValue("dir_name", $dir->getDirName(), 'string');
        $stmt->bindValue("is_root", $dir->getisRoot(), 'integer');
        $stmt->bindValue("parent_id", $dir->getParentId(), 'integer');
        $stmt->bindValue("type_id", $dir->getTypeId(), 'integer');
        $stmt->execute();

        return $this->database->lastInsertId();
    }

    public function isRootDir($userId) {
        $sql = "SELECT COUNT(*) AS count FROM dir WHERE user_id = :user_id AND is_root = 1";
        $stmt = $this->database->prepare($sql);
        $stmt->bindValue("user_id", $userId, 'integer');
        $stmt->execute();

        $dircount = $stmt->fetch();

        if ($dircount['count'] == 0){
            return 1;
        }

        return 0;
    }

    public function getRootDirs($userId)
    {
        $sql = "SELECT * FROM dir WHERE is_root = 1 AND user_id = :user_id";
        $stmt = $this->database->prepare($sql);
        $stmt->bindValue("user_id", $userId, 'integer');
        $stmt->execute();

        $dirs = $stmt->fetchAll();

        return $dirs;
    }

    public function getDirsByParent($userId, $parentId) {
        $sql = "SELECT * FROM dir WHERE user_id = :user_id AND parent_id = :parent_id";
        $stmt = $this->database->prepare($sql);
        $stmt->bindValue("user_id", $userId, 'integer');
        $stmt->bindValue("parent_id", $parentId, 'integer');
        $stmt->execute();

        $dirs = $stmt->fetchAll();

        return $dirs;
    }

    public function checkRegisteredUser($username) {
        $sql = "SELECT COUNT(*) AS count FROM user WHERE username = :username";
        $stmt = $this->database->prepare($sql);
        $stmt->bindValue("username", $username, 'string');
        $stmt->execute();

        $usercount = $stmt->fetch();

        if ($usercount['count'] == 0){
            return FALSE;
        }

        return TRUE;
    }

    public function checkRegisteredEmail($email) {
        $sql = "SELECT COUNT(*) AS count FROM user WHERE email = :email";
        $stmt = $this->database->prepare($sql);
        $stmt->bindValue("email", $email, 'string');
        $stmt->execute();

        $usercount = $stmt->fetch();

        if ($usercount['count'] == 0){
            return FALSE;
        }

        return TRUE;
    }

    public function shareDir($dirId, $userId, $role)
    {
        $sql = "INSERT INTO `share` (`dir_id`, `user_id`, `role`) VALUES (:dir_id, :user_id, :role)";
        $stmt = $this->database->prepare($sql);
        $stmt->bindValue("dir_id", $dirId, 'integer');
        $stmt->bindValue("user_id", $userId, 'integer');
        $stmt->bindValue("role", $role, 'integer');
        $stmt->execute();
    }

    public function getUserIdByEmail($email) {
        $sql = "SELECT id FROM user WHERE email = :email";
        $stmt = $this->database->prepare($sql);
        $stmt->bindValue("email", $email, 'string');
        $stmt->execute();

        $user = $stmt->fetch();

        return $user['id'];
    }

    public function getSharedDirs($userId) {
        $sql = "SELECT * FROM dir WHERE id IN (SELECT dir_id FROM share WHERE user_id = :user_id) AND is_root = 1";
        $stmt = $this->database->prepare($sql);
        $stmt->bindValue("user_id", $userId, 'integer');
        $stmt->execute();

        $dirs = $stmt->fetchAll();

        foreach($dirs as $key=>$dir){
            if ($this->isAdminRole($dir['id'], $userId)) {
                $isAdmin = TRUE;
            } else {
                $isAdmin = FALSE;
            }
            $dirs[$key]['isAdmin'] = $isAdmin;
        }

        return $dirs;
    }

    public function isSharedDirsByParent($userId, $parentId, $role) {
        if ($role == 1) {
            $sql = "SELECT COUNT(*) AS count FROM dir WHERE parent_id = :parent_id AND parent_id IN (SELECT dir_id FROM share WHERE user_id = :user_id)";
        } else {
            $sql = "SELECT COUNT(*) AS count FROM dir WHERE parent_id = :parent_id AND parent_id IN (SELECT dir_id FROM share WHERE user_id = :user_id AND role = 2)";
        }
        $stmt = $this->database->prepare($sql);
        $stmt->bindValue("user_id", $userId, 'integer');
        $stmt->bindValue("parent_id", $parentId, 'integer');
        $stmt->execute();

        $verification = $stmt->fetch();

        if ($verification['count'] == 0){
            return FALSE;
        }

        return TRUE;
    }

    public function getDirParentId($dirId) {
        $sql = "SELECT parent_id FROM dir WHERE id = :dir_id";
        $stmt = $this->database->prepare($sql);
        $stmt->bindValue("dir_id", $dirId, 'integer');
        $stmt->execute();

        $dir = $stmt->fetch();

        return $dir['parent_id'];
    }

    public function deleteDir($dirId, $type) {
        switch($type) {
            case 1:
                //Folder
                $sql = "DELETE FROM dir WHERE id = :dir_id";
                $typeName = 'integer';
                break;
            case 2:
                //File
                $sql = "DELETE FROM dir WHERE dir_name = :dir_id";
                $typeName = 'string';
                break;
        }
        $stmt = $this->database->prepare($sql);
        $stmt->bindValue("dir_id", $dirId, $typeName);
        $stmt->execute();
    }

    public function deleteUser($userId) {
        $sql = "DELETE FROM user WHERE id = :user_id";
        $stmt = $this->database->prepare($sql);
        $stmt->bindValue("user_id", $userId, 'integer');
        $stmt->execute();
    }

    public function deleteUserDirs($userId) {
        $sql = "DELETE FROM dir WHERE user_id = :user_id";
        $stmt = $this->database->prepare($sql);
        $stmt->bindValue("user_id", $userId, 'integer');
        $stmt->execute();
    }

    public function deleteUserShares($userId) {
        $sql = "DELETE FROM share WHERE user_id = :user_id";
        $stmt = $this->database->prepare($sql);
        $stmt->bindValue("user_id", $userId, 'integer');
        $stmt->execute();
    }

    public function deleteUserVerificationKeys($userId) {
        $sql = "DELETE FROM verification_key WHERE user_id = :user_id";
        $stmt = $this->database->prepare($sql);
        $stmt->bindValue("user_id", $userId, 'integer');
        $stmt->execute();
    }

    public function insertVerificationKey($userId, $key) {
        $sql = "INSERT INTO verification_key(user_id, verification_key) VALUES(:user_id, :verification_key)";
        $stmt = $this->database->prepare($sql);
        $stmt->bindValue("user_id", $userId, 'integer');
        $stmt->bindValue("verification_key", $key, 'string');
        $stmt->execute();
    }

    public function checkVerification($key) {
        $sql = "SELECT COUNT(*) AS count, user_id FROM verification_key WHERE verification_key = :verification_key GROUP BY user_id";
        $stmt = $this->database->prepare($sql);
        $stmt->bindValue("verification_key", $key, 'string');
        $stmt->execute();

        $verification = $stmt->fetch();

        if ($verification['count'] == 0){
            return FALSE;
        }

        return $verification['user_id'];
    }

    public function updateVerified($userId)
    {
        $sql = "UPDATE user SET verified = 1 WHERE id = :user_id";
        $stmt = $this->database->prepare($sql);
        $stmt->bindValue("user_id", $userId, 'integer');
        $stmt->execute();
    }

    public function updateProfileImage($userId, $profileImage) {
        $sql = "UPDATE user SET profile_image = :profile_image WHERE id = :user_id";
        $stmt = $this->database->prepare($sql);
        $stmt->bindValue("user_id", $userId, 'integer');
        $stmt->bindValue("profile_image", $profileImage, 'string');
        $stmt->execute();
    }

    public function getUserProfileImage($userId) {
        $sql = "SELECT profile_image FROM user WHERE id = :user_id";
        $stmt = $this->database->prepare($sql);
        $stmt->bindValue("user_id", $userId, 'integer');
        $stmt->execute();

        $user = $stmt->fetch();

        return $user['profile_image'];
    }

    public function userHasRole($userId, $dirId, $roleId) {
        $sql = "SELECT COUNT(*) AS count FROM share WHERE user_id = :user_id AND dir_id = :dir_id AND role = :role";
        $stmt = $this->database->prepare($sql);
        $stmt->bindValue("user_id", $userId, 'integer');
        $stmt->bindValue("dir_id", $dirId, 'integer');
        $stmt->bindValue("role", $roleId, 'integer');
        $stmt->execute();

        $verification = $stmt->fetch();

        if ($verification['count'] == 0){
            return FALSE;
        }

        return TRUE;
    }

    public function incrementStorage($userId, $fileSize) {
        $sql = "UPDATE user SET storage = storage + :fileSize WHERE id = :user_id";
        $stmt = $this->database->prepare($sql);
        $stmt->bindValue("user_id", $userId, 'integer');
        $stmt->bindValue("fileSize", $fileSize, 'integer');
        $stmt->execute();
    }

    public function decrementStorage($userId, $fileSize) {
        $sql = "UPDATE user SET storage = storage - :fileSize WHERE id = :user_id";
        $stmt = $this->database->prepare($sql);
        $stmt->bindValue("user_id", $userId, 'integer');
        $stmt->bindValue("fileSize", $fileSize, 'integer');
        $stmt->execute();
    }

    public function newNotification($userId, $message) {
        $sql = "INSERT INTO notification(user_id, message) VALUES(:user_id, :message)";
        $stmt = $this->database->prepare($sql);
        $stmt->bindValue("user_id", $userId, 'integer');
        $stmt->bindValue("message", $message, 'string');
        $stmt->execute();
    }

    public function getNotifications($userId) {
        $sql = "SELECT * FROM notification WHERE user_id = :user_id ORDER BY id DESC";
        $stmt = $this->database->prepare($sql);
        $stmt->bindValue("user_id", $userId, 'integer');
        $stmt->execute();

        $notifications = $stmt->fetchAll();

        return $notifications;
    }

    public function getSharedOwner($parentId) {
        $sql = "SELECT user_id FROM dir WHERE id = :parent_id";
        $stmt = $this->database->prepare($sql);
        $stmt->bindValue("parent_id", $parentId, 'integer');
        $stmt->execute();

        $user = $stmt->fetch();

        return $user['user_id'];
    }

    public function updateDirName($dirId, $name, $type) {
        if ($type == 1) {
            $sql = "UPDATE dir SET name = :name WHERE id = :dir_id";
            $format = "integer";
        } else if ($type == 2) {
            $sql = "UPDATE dir SET name = :name WHERE dir_name = :dir_id";
            $format = "string";
        }
        $stmt = $this->database->prepare($sql);
        $stmt->bindValue("dir_id", $dirId, $format);
        $stmt->bindValue("name", $name, 'string');
        $stmt->execute();
    }

    public function isAdminRole($dirId, $userId) {
        $sql = "SELECT COUNT(*) AS count FROM share WHERE dir_id = :dir_id AND user_id = :user_id AND role = 2";
        $stmt = $this->database->prepare($sql);
        $stmt->bindValue("user_id", $userId, 'integer');
        $stmt->bindValue("dir_id", $dirId, 'integer');
        $stmt->execute();

        $isAdmin = $stmt->fetch();

        if ($isAdmin['count'] == 0){
            return FALSE;
        }

        return TRUE;
    }
}