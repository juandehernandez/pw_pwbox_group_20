<?php
/**
 * Created by PhpStorm.
 * User: juandelacruz
 * Date: 14/5/18
 * Time: 17:06
 */

namespace Pwbox\model\Implementation;


use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;
use Pwbox\model\MailerRepository;

class PhpMailerRepository implements MailerRepository
{
    private $mail;

    public function __construct(PHPMailer $mail)
    {
        $this->mail = $mail;
    }

    public function sendMail($username, $to, $message) {
        try {
            //Recipients
            $this->mail->setFrom('newpwbox@outlook.com', 'Mailer');
            $this->mail->addAddress($to, $username);     // Add a recipient

            //Content
            $this->mail->isHTML(true);                                  // Set email format to HTML
            $this->mail->Subject = 'Pwbox';
            $this->mail->Body    = $message;
            $this->mail->AltBody    = $message;

            $this->mail->send();
        } catch (Exception $e) {
            echo 'Message could not be sent. Mailer Error: ', $this->mail->ErrorInfo;
        }
    }
}