<?php
/**
 * Created by PhpStorm.
 * User: juandelacruz
 * Date: 6/5/18
 * Time: 19:24
 */

namespace Pwbox\model\Services;


use Pwbox\model\UserRepository;

class GetRootDirsService
{
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * LoginUserService constructor.
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke($userId)
    {
        return $this->repository->getRootDirs($userId);
    }
}