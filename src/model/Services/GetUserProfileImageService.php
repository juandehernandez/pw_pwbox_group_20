<?php
/**
 * Created by PhpStorm.
 * User: juandelacruz
 * Date: 16/5/18
 * Time: 16:44
 */

namespace Pwbox\model\Services;


use Pwbox\model\UserRepository;

class GetUserProfileImageService
{
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * GetUserProfileImageService constructor.
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke($userId)
    {
        return $this->repository->getUserProfileImage($userId);
    }
}