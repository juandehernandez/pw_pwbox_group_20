<?php
/**
 * Created by PhpStorm.
 * User: juandelacruz
 * Date: 14/5/18
 * Time: 13:50
 */

namespace Pwbox\model\Services;


use Pwbox\model\UserRepository;

class DeleteUserService
{
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * DeleteUserService constructor.
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(array $rawData)
    {
        $this->repository->deleteUserVerificationKeys($rawData['id']);
        $this->repository->deleteUserShares($rawData['id']);
        $this->repository->deleteUserDirs($rawData['id']);
        $this->repository->deleteUser($rawData['id']);
    }
}