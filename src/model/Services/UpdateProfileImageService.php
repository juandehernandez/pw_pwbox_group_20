<?php
/**
 * Created by PhpStorm.
 * User: juandelacruz
 * Date: 16/5/18
 * Time: 15:52
 */

namespace Pwbox\model\Services;


use Pwbox\model\UserRepository;

class UpdateProfileImageService
{
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * UpdateVerifiedService constructor.
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke($userId, $profileImage)
    {
        $this->repository->updateProfileImage($userId, $profileImage);
    }
}