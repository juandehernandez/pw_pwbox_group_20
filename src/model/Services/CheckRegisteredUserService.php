<?php
/**
 * Created by PhpStorm.
 * User: juandelacruz
 * Date: 7/5/18
 * Time: 16:00
 */

namespace Pwbox\model\Services;


use Pwbox\model\UserRepository;

class CheckRegisteredUserService
{
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * LoginUserService constructor.
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke($username)
    {

        return $this->repository->checkRegisteredUser($username);
    }
}