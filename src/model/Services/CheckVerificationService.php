<?php
/**
 * Created by PhpStorm.
 * User: juandelacruz
 * Date: 14/5/18
 * Time: 14:58
 */

namespace Pwbox\model\Services;


use Pwbox\model\UserRepository;

class CheckVerificationService
{
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * LoginUserService constructor.
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke($key)
    {

        return $this->repository->checkVerification($key);
    }
}