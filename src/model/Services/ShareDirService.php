<?php
/**
 * Created by PhpStorm.
 * User: juandelacruz
 * Date: 8/5/18
 * Time: 13:56
 */

namespace Pwbox\model\Services;


use Pwbox\model\UserRepository;

class ShareDirService
{
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * PostUserService constructor.
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(array $rawData)
    {
        //id 1 is "reader"
        //id 2 is "admin"

        if ($rawData['role'] == "reader") {
            $roleId = 1;
        } else if ($rawData['role'] == "admin") {
            $roleId = 2;
        }

        $this->repository->shareDir($rawData['shareDirId'], $rawData['userId'], $roleId);
    }
}