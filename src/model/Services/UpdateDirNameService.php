<?php
/**
 * Created by PhpStorm.
 * User: juandelacruz
 * Date: 18/5/18
 * Time: 15:31
 */

namespace Pwbox\model\Services;


use Pwbox\model\UserRepository;

class UpdateDirNameService
{
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * PostUserService constructor.
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(array $rawData)
    {

        if ((!is_int($rawData['renameDirId']) ? (ctype_digit($rawData['renameDirId'])) : true ) == false) {
            //If Id is not an int, it is a file (the dir_name)
            $this->repository->updateDirName($rawData['renameDirId'], $rawData['dirname'], 2);
        } else {
            $this->repository->updateDirName($rawData['renameDirId'], $rawData['dirname'], 1);
        }

    }
}