<?php
/**
 * Created by PhpStorm.
 * User: juandelacruz
 * Date: 18/5/18
 * Time: 14:27
 */

namespace Pwbox\model\Services;


use Pwbox\model\UserRepository;

class NewNotificationService
{
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * NewNotificationService constructor.
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke($userId, $message)
    {
        $this->repository->newNotification($userId, $message);
    }
}