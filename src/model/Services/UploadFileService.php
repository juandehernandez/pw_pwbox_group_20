<?php
/**
 * Created by PhpStorm.
 * User: juandelacruz
 * Date: 3/5/18
 * Time: 19:32
 */

namespace Pwbox\model\Services;

use Pwbox\model\Dir;
use Pwbox\model\UserRepository;

class UploadFileService
{
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * LoginUserService constructor.
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke($userId, array $files, $directory, $type, $isRoot = null, $parentId = 0)
    {

        $dir_name = bin2hex(openssl_random_pseudo_bytes(8));

        foreach ($files['files'] as $uploadedFile) {


            if ($uploadedFile->getError() !== UPLOAD_ERR_OK) {
                $errors[] = sprintf(

                    'An unexpected error ocurred uploading the file %s',

                    $uploadedFile->getClientFilename()

                );

                continue;
            }

            $fileName = $uploadedFile->getClientFilename();

            $fileInfo = pathinfo($fileName);


            $extension = $fileInfo['extension'];



            if (!$this->isValidExtension($extension, $type)) {

                $errors[] = sprintf(

                    'Unable to upload the file %s, the extension %s is not valid',

                    $fileName,

                    $extension

                );

                return $errors;

            }

            //Check file size is not more than 2MB (2MB = 2097152 Bytes)
            if ($uploadedFile->getSize() > 2097152) {
                $errors[] = sprintf(

                    'Unable to upload the file %s, maximum file size is 2MB.',

                    $fileName

                );

                return $errors;
            }


            //Check storage is not more than 1GB (1GB = 1073741824 Bytes)
            $user = $this->repository->getUserById($userId);
            if ($user['storage'] + $uploadedFile->getSize() > 1073741824) {
                $errors[] = sprintf(

                    'Unable to upload the file %s, you dont have more space, only 1GB.',

                    $fileName

                );

                return $errors;
            }

            $full_dir_name = $dir_name . "." . $extension;

            $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $full_dir_name);

            $this->repository->incrementStorage($userId, $uploadedFile->getSize());

            if ($type === "profileimage") {
                return $full_dir_name;
            } else if ($type === "fileupload") {
                if ($isRoot === 1) {
                    $file = new Dir($userId, $fileName, $full_dir_name, $isRoot, 0, 2);
                } else if ($isRoot === 0) {
                    $file = new Dir($userId, $fileName, $full_dir_name, $isRoot, $parentId, 2);
                }
                $this->repository->insertDir($file);
            }
        }
    }

    /**

     * Validate the extension of the file

     *

     * @param string $extension

     * @return boolean

     */

    private function isValidExtension(string $extension, $type)

    {

        if ($type === "profileimage") {
            $validExtensions = ['jpg', 'png', 'gif', 'zip'];
        } else if ($type === "fileupload") {
            $validExtensions = ['pdf', 'jpg', 'png', 'gif', 'md', 'txt'];
        }

        return in_array($extension, $validExtensions);

    }
}