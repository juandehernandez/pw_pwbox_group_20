<?php
/**
 * Created by PhpStorm.
 * User: juandelacruz
 * Date: 14/5/18
 * Time: 21:47
 */

namespace Pwbox\model\Services;


use Pwbox\model\UserRepository;

class UpdateVerifiedService
{
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * UpdateVerifiedService constructor.
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke($userId)
    {
        $this->repository->updateVerified($userId);
    }
}