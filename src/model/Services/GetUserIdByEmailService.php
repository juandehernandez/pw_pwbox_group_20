<?php
/**
 * Created by PhpStorm.
 * User: juandelacruz
 * Date: 8/5/18
 * Time: 15:36
 */

namespace Pwbox\model\Services;


use Pwbox\model\UserRepository;

class GetUserIdByEmailService
{
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * LoginUserService constructor.
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(array $rawData)
    {
        return $this->repository->getUserIdByEmail($rawData['email']);
    }
}