<?php
/**
 * Created by PhpStorm.
 * User: juandelacruz
 * Date: 13/4/18
 * Time: 23:23
 */

namespace Pwbox\model\Services;

use Pwbox\model\UserRepository;

class LoginUserService
{
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * LoginUserService constructor.
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(array $rawData)
    {

        return $this->repository->login($rawData['email'], md5($rawData['password']));
    }
}