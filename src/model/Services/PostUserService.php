<?php
/**
 * Created by PhpStorm.
 * User: juandelacruz
 * Date: 9/4/18
 * Time: 23:31
 */

namespace Pwbox\model\Services;

use Pwbox\model\User;
use Pwbox\model\UserRepository;

class PostUserService
{
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * PostUserService constructor.
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(array $rawData, $profileImage)
    {

        $now = new \DateTime('now');
        $user = new User(
            null,
            $rawData['username'],
            $rawData['email'],
            md5($rawData['password']),
            $now,
            $now,
            $now,
            $profileImage
        );

        return $this->repository->register($user);
    }
}