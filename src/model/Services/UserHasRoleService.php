<?php
/**
 * Created by PhpStorm.
 * User: juandelacruz
 * Date: 17/5/18
 * Time: 14:26
 */

namespace Pwbox\model\Services;


use Pwbox\model\UserRepository;

class UserHasRoleService
{
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * UserHasRoleService constructor.
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(array $rawData)
    {
        //id 1 is "reader"
        //id 2 is "admin"

        if ($rawData['role'] == "reader") {
            $roleId = 1;
        } else if ($rawData['role'] == "admin") {
            $roleId = 2;
        }

        return $this->repository->userHasRole($rawData['userId'], $rawData['shareDirId'], $roleId);
    }
}