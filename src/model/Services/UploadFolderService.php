<?php
/**
 * Created by PhpStorm.
 * User: juandelacruz
 * Date: 4/5/18
 * Time: 11:31
 */

namespace Pwbox\model\Services;


use Pwbox\model\Dir;
use Pwbox\model\UserRepository;

class UploadFolderService
{
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * LoginUserService constructor.
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke($userId, $folderName, $isRoot, $parentId = 0)
    {
        if ($isRoot == 1) {
            $folder = new Dir($userId, $folderName, "folder", $isRoot, 0,1);
        } else {
            $folder = new Dir($userId, $folderName, "folder", $isRoot, $parentId,1);
        }

        $this->repository->insertDir($folder);
    }
}