<?php
/**
 * Created by PhpStorm.
 * User: juandelacruz
 * Date: 18/5/18
 * Time: 17:08
 */

namespace Pwbox\model\Services;


use Pwbox\model\UserRepository;

class IsAdminRoleService
{
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * IsAdminRoleService constructor.
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke($parentId, $userId)
    {
        //Role 2 (third parameter)
        $isSharedDirsByParent = $this->repository->isSharedDirsByParent($userId, $parentId, 2);

        if ($isSharedDirsByParent) {
            return TRUE;
        }

        $parentId = $this->repository->getDirParentId($parentId);

        while ($parentId != 0) {
            $isSharedDirsByParent = $this->repository->isSharedDirsByParent($userId, $parentId,2);

            if ($isSharedDirsByParent) {
                return TRUE;
            }

            $parentId = $this->repository->getDirParentId($parentId);
        }

        return FALSE;
    }
}