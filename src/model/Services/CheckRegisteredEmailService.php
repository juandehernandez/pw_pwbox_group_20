<?php
/**
 * Created by PhpStorm.
 * User: juandelacruz
 * Date: 8/5/18
 * Time: 13:16
 */

namespace Pwbox\model\Services;


use Pwbox\model\UserRepository;

class CheckRegisteredEmailService
{
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * LoginUserService constructor.
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke($email)
    {

        return $this->repository->checkRegisteredEmail($email);
    }
}