<?php
/**
 * Created by PhpStorm.
 * User: juandelacruz
 * Date: 8/5/18
 * Time: 21:34
 */

namespace Pwbox\model\Services;


use Pwbox\model\UserRepository;

class RemoveDirService
{
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * RemoveDirService constructor.
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke($userId, $dirId, $userdirectory)
    {
        if ((!is_int($dirId) ? (ctype_digit($dirId)) : true ) == false) {
            //If Id is not an int, it is a file (the dir_name)
            $this->repository->deleteDir($dirId, 2);
            $fileSize = filesize($userdirectory . DIRECTORY_SEPARATOR . $dirId);
            $this->repository->decrementStorage($userId, $fileSize);
            unlink($userdirectory . DIRECTORY_SEPARATOR . $dirId);
            return;
        }

        $dirsToDelete = array($dirId);
        while (!empty($dirsToDelete)) {
            foreach ($dirsToDelete as $key=>$dirToDelete) {
                $childs = $this->repository->getDirsByParent($userId, $dirToDelete);
                foreach ($childs as $child) {
                    array_push($dirsToDelete, $child['id']);
                }

                if ((!is_int($dirId) ? (ctype_digit($dirId)) : true ) == false) {
                    $fileSize = filesize($userdirectory . DIRECTORY_SEPARATOR . $dirId);
                    $this->repository->decrementStorage($userId, $fileSize);
                    unlink($userdirectory . DIRECTORY_SEPARATOR . $dirId);

                    $this->repository->deleteDir($dirToDelete, 1);
                } else {
                    $this->repository->deleteDir($dirToDelete, 1);
                }
                unset($dirsToDelete[$key]);
            }
        }


    }
}