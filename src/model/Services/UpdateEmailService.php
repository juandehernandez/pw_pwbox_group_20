<?php
/**
 * Created by PhpStorm.
 * User: juandelacruz
 * Date: 14/4/18
 * Time: 20:51
 */

namespace Pwbox\model\Services;

use Pwbox\model\User;
use Pwbox\model\UserRepository;

class UpdateEmailService
{
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * PostUserService constructor.
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(array $rawData)
    {

        $this->repository->updateEmail($_SESSION['id'], $rawData['email']);

    }
}