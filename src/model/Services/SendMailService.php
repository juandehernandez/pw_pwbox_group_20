<?php
/**
 * Created by PhpStorm.
 * User: juandelacruz
 * Date: 14/5/18
 * Time: 17:29
 */

namespace Pwbox\model\Services;


use Pwbox\model\MailerRepository;

class SendMailService
{
    /**
     * @var MailerRepository
     */
    private $repository;

    /**
     * SendMailService constructor.
     * @param MailerRepository $repository
     */
    public function __construct(MailerRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke($username, $to, $message)
    {
        $this->repository->sendMail($username, $to, $message);
    }
}