<?php
/**
 * Created by PhpStorm.
 * User: juandelacruz
 * Date: 18/5/18
 * Time: 14:50
 */

namespace Pwbox\model\Services;


use Pwbox\model\UserRepository;

class GetSharedOwnerService
{
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * GetSharedOwnerService constructor.
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke($parentId)
    {
        return $this->repository->getSharedOwner($parentId);
    }
}