<?php
/**
 * Created by PhpStorm.
 * User: juandelacruz
 * Date: 6/5/18
 * Time: 19:24
 */

namespace Pwbox\model\Services;


use Pwbox\model\UserRepository;

class GetDirsByParent
{
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * LoginUserService constructor.
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke($userId, $parentId)
    {

        return $this->repository->getDirsByParent($userId, $parentId);
    }
}