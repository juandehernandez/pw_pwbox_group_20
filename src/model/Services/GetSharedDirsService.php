<?php
/**
 * Created by PhpStorm.
 * User: juandelacruz
 * Date: 8/5/18
 * Time: 20:39
 */

namespace Pwbox\model\Services;


use Pwbox\model\UserRepository;

class GetSharedDirsService
{
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * LoginUserService constructor.
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke($userId)
    {

        return $this->repository->getSharedDirs($userId);
    }
}