<?php
/**
 * Created by PhpStorm.
 * User: juandelacruz
 * Date: 20/4/18
 * Time: 22:49
 */

namespace Pwbox\model\Services;

use Pwbox\model\UserRepository;

class UpdatePasswordService
{
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * PostUserService constructor.
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(array $rawData)
    {

        $this->repository->updatePassword($_SESSION['id'], md5($rawData['newPassword']));

    }
}