<?php
/**
 * Created by PhpStorm.
 * User: juandelacruz
 * Date: 15/5/18
 * Time: 1:26
 */

namespace Pwbox\model\Services;


use Pwbox\model\UserRepository;

class GetSharedDirsByParentService
{
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * GetSharedDirsByParentService constructor.
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke($userId, $parentId)
    {
        $firstParentId = $parentId;

        $isSharedDirsByParent = $this->repository->isSharedDirsByParent($userId, $parentId, 1);

        if ($isSharedDirsByParent) {
            return $this->repository->getDirsByParent($userId, $parentId);
        }

        $parentId = $this->repository->getDirParentId($parentId);

        while ($parentId != 0) {
            $isSharedDirsByParent = $this->repository->isSharedDirsByParent($userId, $parentId, 1);

            if ($isSharedDirsByParent) {
                return $this->repository->getDirsByParent($userId, $firstParentId);
            }

            $parentId = $this->repository->getDirParentId($parentId);
        }

        return null;
    }
}