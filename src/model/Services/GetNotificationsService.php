<?php
/**
 * Created by PhpStorm.
 * User: juandelacruz
 * Date: 18/5/18
 * Time: 14:28
 */

namespace Pwbox\model\Services;


use Pwbox\model\UserRepository;

class GetNotificationsService
{
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * GetNotificationsService constructor.
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke($userId)
    {
        return $this->repository->getNotifications($userId);
    }
}