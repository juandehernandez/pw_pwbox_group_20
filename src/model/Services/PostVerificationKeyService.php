<?php
/**
 * Created by PhpStorm.
 * User: juandelacruz
 * Date: 14/5/18
 * Time: 15:11
 */

namespace Pwbox\model\Services;


use Pwbox\model\UserRepository;

class PostVerificationKeyService
{
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * PostUserService constructor.
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke($userId, $verificationKey)
    {
        $this->repository->insertVerificationKey($userId, $verificationKey);
    }
}