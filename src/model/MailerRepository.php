<?php
/**
 * Created by PhpStorm.
 * User: juandelacruz
 * Date: 14/5/18
 * Time: 17:07
 */

namespace Pwbox\model;


interface MailerRepository
{
    public function sendMail($username, $to, $message);
}