<?php
/**
 * Created by PhpStorm.
 * User: juandelacruz
 * Date: 3/5/18
 * Time: 22:51
 */

namespace Pwbox\model;


class Dir
{
    private $user_id;

    private $name;

    private $dir_name;

    private $is_root;

    private $parent_id;

    private $type_id;

    public function __construct(
        $user_id,
        $name,
        $dir_name,
        $is_root,
        $parent_id,
        $type_id
    ) {
        $this->user_id = $user_id;
        $this->name = $name;
        $this->dir_name = $dir_name;
        $this->is_root = $is_root;
        $this->parent_id = $parent_id;
        $this->type_id = $type_id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getChildId()
    {
        return $this->child_id;
    }

    /**
     * @return mixed
     */
    public function getDirName()
    {
        return $this->dir_name;
    }

    /**
     * @return mixed
     */
    public function getisRoot()
    {
        return $this->is_root;
    }

    /**
     * @return mixed
     */
    public function getParentId()
    {
        return $this->parent_id;
    }

    /**
     * @return mixed
     */
    public function getTypeId()
    {
        return $this->type_id;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }
}