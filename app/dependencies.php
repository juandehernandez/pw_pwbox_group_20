<?php

$container = $app->getContainer();

$container['view'] = function($container) {

    $view = new \Slim\Views\Twig(__DIR__ . '/../src/view/templates', [

        //'cache' => __DIR__ . '/../var/cache/'

    ]);

    $basePath = rtrim(str_ireplace('index.php', '', $container['request']->getUri()->getBasePath()), '/');

    $view->addExtension(new \Slim\Views\TwigExtension($container['router'], $basePath));

    $view->getEnvironment()->addGlobal('flash', $container['flash']);

    return $view;

};

$container['doctrine'] = function($container) {
    $config = new \Doctrine\DBAL\Configuration();
    $conn = \Doctrine\DBAL\DriverManager::getConnection(
        $container->get('settings')['database'],
        $config
    );

    return $conn;
};

$container['phpmailer'] = function($container) {
    $settings = $container->get('settings')['mailer'];

    $mail = new \PHPMailer\PHPMailer\PHPMailer(true);                              // Passing `true` enables exceptions
    //Server settings
    $mail->SMTPDebug = 0;                                 // Enable verbose debug output
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = $settings['host'];  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = $settings['username'];                 // SMTP username
    $mail->Password = $settings['password'];                           // SMTP password
    $mail->SMTPSecure = $settings['encryption'];                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = $settings['port'];

    $mail->setFrom($settings['username'], 'Mailer');

    return $mail;
};

$container['user_repository'] = function($container) {
    $repository = new \Pwbox\model\Implementation\DoctrineUserRepository(
        $container->get('doctrine')
    );
    return $repository;
};

$container['mailer_repository'] = function($container) {
    $repository = new \Pwbox\model\Implementation\PhpMailerRepository(
        $container->get('phpmailer')
    );
    return $repository;
};

$container['post_user_service'] = function($container) {
    $service = new \Pwbox\model\Services\PostUserService(
        $container->get('user_repository')
    );

    return $service;
};

$container['login_user_service'] = function($container) {
    $service = new \Pwbox\model\Services\LoginUserService(
        $container->get('user_repository')
    );

    return $service;
};

$container['update_email_service'] = function($container) {
    $service = new \Pwbox\model\Services\UpdateEmailService(
        $container->get('user_repository')
    );

    return $service;
};

$container['flash'] = function(){
    return new \Slim\Flash\Messages();
};

$container['update_password_service'] = function($container) {
    $service = new \Pwbox\model\Services\UpdatePasswordService(
        $container->get('user_repository')
    );

    return $service;
};

$container['get_user_service'] = function($container) {
    $service = new \Pwbox\model\Services\GetUserService(
        $container->get('user_repository')
    );

    return $service;
};

$container['flash'] = function() {
    return new \Slim\Flash\Messages();
};

$container['upload_directory'] = __DIR__ . '/../public/userinfo';


$container['profile_images_directory'] = __DIR__ . '/../public/profileimages';

$container['web_profile_images'] = '/profileimages';

$container['web_default_avatar'] = '/assets/images/default_avatar.png';

$container['upload_file_service'] = function($container) {
    $service = new \Pwbox\model\Services\UploadFileService(
        $container->get('user_repository')
    );

    return $service;
};

$container['upload_folder_service'] = function($container) {
    $service = new \Pwbox\model\Services\UploadFolderService(
        $container->get('user_repository')
    );

    return $service;
};

$container['get_root_dirs_service'] = function($container) {
    $service = new \Pwbox\model\Services\GetRootDirsService(
        $container->get('user_repository')
    );

    return $service;
};

$container['get_dirs_by_parent_service'] = function($container) {
    $service = new \Pwbox\model\Services\GetDirsByParent(
        $container->get('user_repository')
    );

    return $service;
};

$container['check_registered_user_service'] = function($container) {
    $service = new \Pwbox\model\Services\CheckRegisteredUserService(
        $container->get('user_repository')
    );

    return $service;
};

$container['check_registered_email_service'] = function($container) {
    $service = new \Pwbox\model\Services\CheckRegisteredEmailService(
        $container->get('user_repository')
    );

    return $service;
};

$container['share_dir_service'] = function($container) {
    $service = new \Pwbox\model\Services\ShareDirService(
        $container->get('user_repository')
    );

    return $service;
};

$container['get_userid_by_email_service'] = function($container) {
    $service = new \Pwbox\model\Services\GetUserIdByEmailService(
        $container->get('user_repository')
    );

    return $service;
};

$container['get_shared_dirs_service'] = function($container) {
    $service = new \Pwbox\model\Services\GetSharedDirsService(
        $container->get('user_repository')
    );

    return $service;
};

$container['get_shared_dirs_by_parent_service'] = function($container) {
    $service = new \Pwbox\model\Services\GetSharedDirsByParentService(
        $container->get('user_repository')
    );

    return $service;
};

$container['remove_dir_service'] = function($container) {
    $service = new \Pwbox\model\Services\RemoveDirService(
        $container->get('user_repository')
    );

    return $service;
};

$container['delete_user_service'] = function($container) {
    $service = new \Pwbox\model\Services\DeleteUserService(
        $container->get('user_repository')
    );

    return $service;
};

$container['post_verification_key_service'] = function($container) {
    $service = new \Pwbox\model\Services\PostVerificationKeyService(
        $container->get('user_repository')
    );

    return $service;
};

$container['check_verification_service'] = function($container) {
    $service = new \Pwbox\model\Services\CheckVerificationService(
        $container->get('user_repository')
    );

    return $service;
};

$container['send_mail_service'] = function($container) {
    $service = new \Pwbox\model\Services\SendMailService(
        $container->get('mailer_repository')
    );

    return $service;
};

$container['update_verified_service'] = function($container) {
    $service = new \Pwbox\model\Services\UpdateVerifiedService(
        $container->get('user_repository')
    );

    return $service;
};

$container['update_profile_image_service'] = function($container) {
    $service = new \Pwbox\model\Services\UpdateProfileImageService(
        $container->get('user_repository')
    );

    return $service;
};

$container['get_user_profile_image_service'] = function($container) {
    $service = new \Pwbox\model\Services\GetUserProfileImageService(
        $container->get('user_repository')
    );

    return $service;
};

$container['user_has_role_service'] = function($container) {
    $service = new \Pwbox\model\Services\UserHasRoleService(
        $container->get('user_repository')
    );

    return $service;
};

$container['new_notification_service'] = function($container) {
    $service = new \Pwbox\model\Services\NewNotificationService(
        $container->get('user_repository')
    );

    return $service;
};

$container['get_notifications_service'] = function($container) {
    $service = new \Pwbox\model\Services\GetNotificationsService(
        $container->get('user_repository')
    );

    return $service;
};

$container['get_shared_owner_service'] = function($container) {
    $service = new \Pwbox\model\Services\GetSharedOwnerService(
        $container->get('user_repository')
    );

    return $service;
};

$container['update_dir_name_service'] = function($container) {
    $service = new \Pwbox\model\Services\UpdateDirNameService(
        $container->get('user_repository')
    );

    return $service;
};

$container['is_admin_role_service'] = function($container) {
    $service = new \Pwbox\model\Services\IsAdminRoleService(
        $container->get('user_repository')
    );

    return $service;
};