<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use \Pwbox\controller\RegisterController;


$app->add('\Pwbox\controller\Middleware\SessionMiddleware');

$app->get('/', '\Pwbox\controller\GetRegisterController');

$app->get(
    '/hello/{name}',
    '\Pwbox\controller\HelloController'
)->add('\Pwbox\controller\Middleware\ExampleMiddleWare');

$app->post('/', '\Pwbox\controller\PostRegisterController');

$app->get('/login', '\Pwbox\controller\GetLoginController');

$app->post('/login', '\Pwbox\controller\PostLoginController');

$app->get('/profile', '\Pwbox\controller\GetProfileController')->add('\Pwbox\controller\Middleware\UserLoggedMiddleware');

$app->post('/profile', '\Pwbox\controller\PostProfileController')->add('\Pwbox\controller\Middleware\UserLoggedMiddleware');;

$app->get('/dashboard[/{folder}]', '\Pwbox\controller\GetDashboardController')->add('\Pwbox\controller\Middleware\UserLoggedMiddleware');

$app->post('/dashboard[/{folder}]', '\Pwbox\controller\PostDashboardController')->add('\Pwbox\controller\Middleware\UserLoggedMiddleware');;

$app->get('/shared[/{folder}]', '\Pwbox\controller\GetSharedController')->add('\Pwbox\controller\Middleware\UserLoggedMiddleware');

$app->post('/shared[/{folder}]', '\Pwbox\controller\PostSharedController')->add('\Pwbox\controller\Middleware\UserLoggedMiddleware');

$app->get('/mail_activation[/{key}]', '\Pwbox\controller\MailActivationController');

$app->get('/logout', function(Request $request, Response $response) {
    $_SESSION['id'] = null;
    return $response->withStatus(302)->withHeader('Location', '/');
});

$app->get('/notifications', '\Pwbox\controller\GetNotificationsController')->add('\Pwbox\controller\Middleware\UserLoggedMiddleware');